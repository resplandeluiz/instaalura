import { Platform } from 'react-native'

export const login = async (userName, password) => {

    let url = '10.0.2.2'

    if (Platform.OS === 'ios') uri = "localhost";

    const headerHTTP = {
        method: "POST",
        body: JSON.stringify({ userName, password }),
        headers: { "Content-type": "application/json" }
    }

    let response = await fetch(`http://${url}:3030/users/login`, headerHTTP)
    let responseJson = await response.json()
    
    if (response.ok)
        return response.headers.get("x-access-token")
    else
        throw new Error(responseJson.message)
        

}