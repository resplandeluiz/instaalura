export const likeImage = (likeFlag, totalLikes) => {

    let varLikes = totalLikes

    if (likeFlag) {
        if (!(totalLikes <= 0)) varLikes--;
    } else
        varLikes++;

    return [!likeFlag, varLikes]
}

export const renderImageLike = (like) => {
    if (like)
        return require('../../resources/img/s2-checked.png')
    else
        return require('../../resources/img/s2.png')
}