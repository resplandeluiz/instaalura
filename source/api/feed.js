import { Platform } from 'react-native'


export const getData = async (callback) => {
    
    let url = '10.0.2.2'

    if (Platform.OS === 'ios') uri = "localhost";

    let response = await fetch(`http://${url}:3030/feed`)
    let responseJson = await response.json()
    
    callback(responseJson)

}