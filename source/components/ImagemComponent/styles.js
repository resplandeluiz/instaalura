import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get("screen");

export default styles = StyleSheet.create({

    img: { width: width, height: width },
    likeButton: { width: 50, height: 50, margin: 5 },
    viewLike: { flexDirection: "row", alignItems: "center" }

})
