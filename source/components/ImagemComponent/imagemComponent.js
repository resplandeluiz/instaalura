import React, { Fragment, useState } from 'react'
import { Image, Text, TouchableOpacity, View } from 'react-native'
import styles from './styles'


export default ImagemComponent = ({
    image,
    descricao,
    likes,
    likeImage,
    renderImageLike
}) => {

    const [likeFlag, setLikeFlag] = useState(false)
    const [totalLikes, setTotalLikes] = useState(likes)

    const callLikeImage = () => {
        let [resFlag, resLikes] = likeImage(likeFlag, totalLikes)
        setTotalLikes(resLikes)
        setLikeFlag(resFlag)
    }

    return (
        <Fragment>

            <Image source={{ uri: image }} style={styles.img} />
            <View style={{ paddingLeft: 10 }}>
                <Text >{descricao}</Text>
            </View>

            <View style={styles.viewLike}>

                <TouchableOpacity onPress={callLikeImage}>
                    <Image source={renderImageLike(likeFlag)} style={styles.likeButton} />
                </TouchableOpacity>


                <Text>curtidas {totalLikes}</Text>

            </View>

        </Fragment>
    )
}
