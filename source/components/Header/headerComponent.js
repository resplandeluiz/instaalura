
import React from 'react'
import { Text, View, Image } from 'react-native';
import styles from './styles';

export default HeaderComponent = ({ texto, image }) => {

    return (

        <View style={styles.header}>

            <Image source={{ uri: image }} style={styles.imagemPerfil} />
            <Text style={styles.text}>{texto}</Text>

        </View>

    )

}



