import { StyleSheet } from 'react-native'


export default styles = StyleSheet.create({
    boxTextComent: {
        flexDirection: "row",      
        alignItems: "center"
    },
    imgComent: {
        width: 30,
        height: 30,
        marginRight: 5
    }
})