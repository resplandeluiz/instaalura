import React, { Fragment, useEffect, useState } from 'react'
import { FlatList, Text, TextInput, Image, View, TouchableOpacity } from 'react-native'
import styles from './styles'

export default ComentComponent = ({ comentarios, addComent }) => {

    let textInput = '';
    let fieldInput;

    const [coments, setComents] = useState(comentarios);

    return (
        <Fragment>

            <FlatList

                data={coments}

                renderItem={({ item }) =>
                    <View style={[styles.boxTextComent, { paddingLeft: 10 }]}>
                        <Text style={{ fontWeight: "bold", marginRight: 10 }}>{item.userName}</Text>
                        <Text>{item.text}</Text>
                    </View>}

                keyExtractor={(item, index) => `coment_${index}`}

            />

            <View style={styles.boxTextComent}>

                <TextInput
                    ref={ref => fieldInput = ref}
                    onChangeText={text => textInput = text}
                    placeholder="Deixe seu comentário..."
                    style={{ flex: 1 }} />

                <TouchableOpacity onPress={() => addComent(fieldInput, textInput, coments, setComents)}>
                    <Image source={require('../../../resources/img/send.png')} style={styles.imgComent} />
                </TouchableOpacity>

            </View>

        </Fragment>
    )
}

