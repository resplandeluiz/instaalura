import React, { Fragment, useEffect, useState } from 'react'
import { FlatList, StatusBar, View } from 'react-native'

import { HeaderComponent } from '../../components/Header/index'
import { ImagemComponent } from '../../components/ImagemComponent/index'
import { ComentComponent } from '../../components/Coments/index'

import { getData } from '../../api/feed'

import { likeImage, renderImageLike } from '../../api/likes'
import { addComent } from '../../api/coment'

const Feed = () => {

  const [data, setData] = useState([])

  useEffect(() => { getData(setData) }, [])

  return (
    <View>

      <StatusBar
        backgroundColor={"#FFFFFF"}
        barStyle={"dark-content"} />

      <FlatList

        data={data}

        renderItem={({ item }) =>

          <Fragment>

            <HeaderComponent texto={item.userName} image={item.userURL} />

            <ImagemComponent image={item.url} descricao={item.description} likes={item.likes} likeImage={likeImage} renderImageLike={renderImageLike} />

            <ComentComponent comentarios={item.comentarios} addComent={addComent} />

          </Fragment>
        }

        keyExtractor={item => item.id.toString()}

      />
    </View>

  );
};

Feed.navigationOptions = ({ navigation }) => {

  const options = { title: navigation.getParam("nome") }

  // if (Platform.OS == "android") options.header = null

  return options

}



export default Feed;

