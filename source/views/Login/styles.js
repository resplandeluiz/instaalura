import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get("screen")

export default style = StyleSheet.create({

    container: {
        flexGrow: 2,
        justifyContent: "center",
        alignItems: "center"
    },

    input: {
        width: width * 0.8,
        color:"#000"
    },

    button: {
        alignItems: "center",
        marginBottom: 50
    }



})