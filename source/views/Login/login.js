import React, { Fragment, useState, useEffect } from 'react'
import AsyncStorage from '@react-native-community/async-storage';

import { View, Text, TextInput, Button, Platform } from 'react-native'
import { login } from '../../api/login'
import styles from './styles'


const Login = ({ navigation }) => {

    const [user, setUser] = useState("")
    const [password, setPassword] = useState("")
    const [messageError, setMessageError] = useState(null)

    const tryLogin = async () => {

        try {

            const token = await login(user, password)
            await AsyncStorage.setItem("tokenApi", token)
            navigation.replace("Feed", { nome: user })

        } catch (e) {
            setMessageError(e.message)
        }

    }



    return (
        <Fragment>

            <View style={styles.container}>

                <TextInput
                    placeholder={"User"}
                    onChangeText={text => setUser(text)}
                    style={styles.input} />

                <TextInput
                    placeholder={"Password"}
                    style={styles.input}
                    onChangeText={text => setPassword(text)}
                    secureTextEntry={true} />


                {messageError && <Text>{messageError}</Text>}

            </View>

            <Button title={"Entrar"} style={styles.button} onPress={tryLogin} />

        </Fragment>
    );
};

Login.navigationOptions = () => {

    const options = { title: "Login" }

    if (Platform.OS == "android") options.header = null

    return options

}

export default Login;

