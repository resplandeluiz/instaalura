import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import Feed from './source/views/Feed/feed'
import Login from './source/views/Login/login'

const nav = createStackNavigator({
    Login: { screen: Login },
    Feed: { screen: Feed },
})

const AppContainer = createAppContainer(nav)

const App = () => <AppContainer />

export default App;

